import requests


def post_file(file, url):
    headers = {'Content-Type': 'application/json'}
    with open(file, 'r') as file_handler:
        json = file_handler.read()
    return requests.post(url, data=json, headers=headers)
