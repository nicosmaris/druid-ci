from functions import post_file

import requests


# Example response from Druid:
# [ {
#     "timestamp" : "2015-09-12T00:46:58.771Z",
#     "result" : [ {
#         "edits" : 33,
#         "page" : "Wikipedia:Vandalismusmeldung"
#     }, {
#         "edits" : 28,
#         "page" : "User:Cyde/List of candidates for speedy deletion/Subpage"
#     }, {
#         "edits" : 27,
#         "page" : "Jeremy Corbyn"
#     }, {
#         "edits" : 21,
#         "page" : "Wikipedia:Administrators' noticeboard/Incidents"
#     }, {
#         "edits" : 20,
#         "page" : "Flavia Pennetta"
#     } ]
# } ]

def test_datasources(druid_host):
    url = "http://{0}:8081/druid/coordinator/v1/metadata/datasources" \
        .format(druid_host)
    response = requests.get(url)
    json = response.json()
    assert json[0] == 'wikiticker'


def test_wikiticker_top_pages(druid_host):
    expected = [
        {
            u'edits': 33,
            u'page': u'Wikipedia:Vandalismusmeldung'
        },
        {
            u'edits': 28,
            u'page':
                u'User:Cyde/List of candidates for speedy deletion/Subpage'
        },
        {
            u'edits': 27,
            u'page': u'Jeremy Corbyn'
        },
        {
            u'edits': 21,
            u'page': u"Wikipedia:Administrators' noticeboard/Incidents"
        },
        {
            u'edits': 20,
            u'page': u'Flavia Pennetta'
        }
    ]

    # Fire the request to druid
    response = post_file(
        "wikiticker-top-pages.json",
        "http://{0}:8082/druid/v2".format(druid_host)
    )

    # Extract the json from the result
    json = response.json()

    print(json)

    assert json[0]['result'] == expected
