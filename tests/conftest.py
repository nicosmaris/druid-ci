import pytest


def pytest_addoption(parser):
    parser.addoption("--druid-host",
                     action="store",
                     default="localhost",
                     help="Host that runs the Druid services")


@pytest.fixture
def druid_host(request):
    return request.config.getoption("--druid-host")
